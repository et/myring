# MYring

Matlab code for Lieb optimization, Moreau-Yosida regularization, and
formal Kohn-Sham iterations for a quantum ring with two electrons.