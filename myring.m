%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MYring - a Matlab program for numerical exploration
%          of Moreau-Yosida regularized paramagnetic
%          current-density-functional theory in the
%          setting of a 1-dimensional quantum ring.
%
% Written by Erik Tellgren, University of Oslo, 2018-2019
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function myring(choicetxt,param,pot_spec)
    if strcmp(choicetxt,'ks-iter')
        mu_reg = param
        example_ks_iter(mu_reg,pot_spec);
    elseif strcmp(choicetxt,'E1-vs-E0')
        example_E1_vs_E0();
    elseif strcmp(choicetxt,'curdep-ueg')
        example_uniform_dens();
    elseif strcmp(choicetxt,'curdep-of-F')
        example_curdep_of_F();
    end
    
    %example_gs_calcs(3);
    %example_lieb_opt(1);
    %example_lieb_grad_test()
end

function example_ks_iter(mu_reg, pot_example)
    % Input: mu_reg = Moreau-Yosida regularization parameter
    
    % model parameters
    R = 1;
    m_e = 1;
    Ngrid = 30;
    hR = 2*pi*R/Ngrid;
    Theta = linspace(0, 2*pi*(1-1/Ngrid), Ngrid)';
    [TH1m, TH2m] = meshgrid(Theta, Theta);
    Wee = 3*sqrt(1 + cos(TH1m - TH2m));

    % other parameters
    use_restart_file = true;
    ls_t_interpolation = true;
    
    
    % log output in file
    fbase = sprintf('ks_maxt_%s_mu%.2f_%dpnts_lowf',pot_example,mu_reg,Ngrid);
    filename = strcat(fbase, '.log');
    if exist(filename,'file')
        delete filename;
    end
    diary(filename);
    date
    
    restart_file = strcat(fbase, '_restart.dat');
    outfilename = strcat(fbase, '.out');
    if use_restart_file
        outf = fopen(outfilename, 'a');
    else
        outf = fopen(outfilename, 'w');
    end
    fprintf(outf,'\n\n---------\nCalculation started %s\n\n',datestr(now));
    
    %%%%%
    if strcmp(pot_example, 'vdouble')
        Vext = cos(2*Theta) + 0.2*cos(Theta);
        Aext = 0.3 + sin(Theta);
    else
        Vext = cos(Theta);
        Aext = 0.6 + 0*sin(Theta);
    end
    Uext = Vext + Aext.^2 / (2*m_e);
    [Psi1,rho1,jp1,E1unreg,Egap1] = gs_2el_iter(Vext,Aext,Wee,R,m_e);
    sigma1 = rho1 - mu_reg*Uext;
    kp1 = jp1 - mu_reg*Aext;
    disp(sprintf('Reference energy (unregularized): E1unreg(Uext,Aext) = %f',E1unreg));
    E1reg = E1unreg - 0.5*mu_reg*hR*sum(Uext.^2+Aext.^2);
    disp(sprintf('Reference energy (regularized): E1reg(Uext,Aext) = %f',E1reg));
    fprintf(outf,'Reference energies: E1unreg(a_ext) = %.16f, E1reg(a_ext) = %.16f\n',E1unreg,E1reg);
    
    % solve for KS system using Lieb optimization;
    % this enables the convergence of the KS potential
    % to be monitored in iterative KS algorithm below
    [U_KS_ref,A_KS_ref,OptHist] = lieb_opt(sigma1,kp1,R,m_e,[],mu_reg,[]);
    
    figure(1); clf; hold on;
    pcolor(Theta,Theta,abs(Psi1).^2);
    axis tight; axis equal;
    xlabel('Particle 1');
    ylabel('Particle 2');
    title('Interacting ground state of H_1(Vext, Aext)');
    
    figure(2); clf; hold on;
    title('External potentials and interacting g.s. densities');
    subplot(2,1,1); hold on;
    plot(Theta, Vext, 'k--');
    plot(Theta, rho1, 'b-');
    axis tight;
    xlabel('\Theta');
    legend('Vext','rho');
    subplot(2,1,2); hold on;
    plot(Theta, Aext, 'm-.');
    plot(Theta, jp1, 'r-');
    axis tight;
    xlabel('\Theta');
    legend('Aext','jp');
    
    % Kohn-Sham iteration
    if use_restart_file & exist(restart_file,'file')
        fprintf(outf,'Taking initial (Utrial,Atrial) from file %s\n',restart_file);
        SKUA = load(restart_file);        
        IX = (size(SKUA,1)-Ngrid+1):size(SKUA,1);
        sigma = SKUA(IX,1);
        kp = SKUA(IX,2);
        Utrial = SKUA(IX,3);
        Atrial = SKUA(IX,4);
        Vtrial = Utrial - Atrial.^2 / (2*m_e);
    else
        Uext = Vext + Aext.^2 / (2*m_e);
        Vtrial = Vext;
        Utrial = Uext;
        Atrial = Aext;

        [Psi_KS,rho_KS,jp_KS,E_KS,E_KSgap] = gs_1el(Vtrial,Atrial,R,m_e);
        sigma = rho_KS - mu_reg*Utrial;
        kp = jp_KS - mu_reg*Atrial;
    end
    
    % gradient of regularized F1
    [Uopt,Aopt,OptHist] = lieb_opt(sigma,kp,R,m_e,Wee,mu_reg,[]);
    F1reg = OptHist(end).lieb_fval;
    sigma = OptHist(end).grad_U / hR + sigma;
    kp = OptHist(end).grad_A / hR + kp;
    
    for it = 1:125
        Q1reg = F1reg + hR*sum(Uext.*sigma + Aext.*kp);
        Q1unreg = Q1reg + 0.5*mu_reg*hR*sum(Uext.^2 + Aext.^2);
        disp(sprintf('KS iteration %d: Q1reg = %f, Q1unreg = %f',it,Q1reg,Q1unreg));
        fprintf(outf,'KS iteration %d: Q1reg = %.16f, Q1unreg = %.16f\n',it,Q1reg,Q1unreg);
        
        if use_restart_file
            restartf = fopen(restart_file, 'a');
            fprintf(restartf,'%%KS iter %d\n %d %d %d %d\n%%sigma kp Utrial Atrial\n',it,it,it,it,it);
            for p = 1:Ngrid
                fprintf(restartf,'%f %f %f %f \n',sigma(p),kp(p),Utrial(p),Atrial(p));
            end
            fclose(restartf);
        end
        
        err_norm_U = norm(Uext - Uopt);
        err_norm_A = norm(Aext - Aopt);
        err_norm_UA = norm([Uext - Uopt; Aext - Aopt]);
        disp(sprintf('KS iteration %d: ||Uopt - Uext|| = %f, ||Aopt - Aext|| = %f, ||aopt - aext|| = %f',it,err_norm_U, err_norm_A, err_norm_UA));
        fprintf(outf,'KS iteration %d: ||Uopt - Uext|| = %f, ||Aopt - Aext|| = %f, ||aopt - aext|| = %f\n',it,err_norm_U, err_norm_A, err_norm_UA);
        err_norm_UA_KS = norm([Utrial - U_KS_ref; Atrial - A_KS_ref]);
        err_norm_dens = norm([sigma - sigma1; kp - kp1]);
        disp(sprintf('KS iteration %d: ||trial pot. - KS ref.pot.|| = %f, ||trial dens. - ref. dens|| = %f',it,err_norm_UA_KS,err_norm_dens));
        fprintf(outf,'KS iteration %d: ||trial pot. - KS ref.pot.|| = %f, ||trial dens. - ref. dens|| = %f\n',it,err_norm_UA_KS,err_norm_dens);
        
        % step (a): auxilary Kohn-Sham potentials
        Uaux = Uext - Uopt + Utrial;
        Aaux = Aext - Aopt + Atrial;
        Vaux = Uaux - Aaux.^2/(2*m_e);
        
        % step (b): densities for KS potentials
        [Psi_prime,rho_prime,jp_prime,E_prime,E_prime_gap] = gs_1el(Vaux,Aaux,R,m_e);
        sigma_prime = rho_prime - mu_reg*Uaux;
        kp_prime = jp_prime - mu_reg*Aaux;

        % step (c): line search
        tmin = 0;
        tmax = 1;
        t = 1;
        step_rejected = true;
        LSHist = [];
        while step_rejected & (tmax - tmin > 1.0e-3 * hR)
            disp(sprintf('KS linesearch: testing monotonicity for t = %f',t));
            fprintf(outf,'KS linesearch: testing monotonicity for t = %f\n',t);
        
            sigma_next = sigma + t * (sigma_prime - sigma);
            kp_next = kp + t * (kp_prime - kp);
        
            % gradient of regularized F1
            [Uopt,Aopt,OptHist] = lieb_opt(sigma_next,kp_next,R,m_e,Wee,mu_reg,[]);
            F1reg = OptHist(end).lieb_fval;
            fprintf(outf,'KS linesearch: F1reg = %f, Egap = %f\n',F1reg,OptHist(end).Egap);
            
            % test that t is small is enough to yield decrease in energy
            sp = (Uext - Uopt)'*(sigma_prime - sigma);
            sp = sp + (Aext - Aopt)'*(kp_prime - kp);
            disp(sprintf('KS linesearch: monotonicity test result is sp = %f',sp));
            fprintf(outf,'KS linesearch: monotonicity test result is sp = %f\n',sp);
            
            LSHist = [LSHist ; t sp];
            if sp <= 0
                step_rejected = false;
            else
                tmax = t;
                t = 0.5*tmin + 0.5*tmax;
            end
        end
        if ls_t_interpolation & (size(LSHist,1) > 1) & (sp > 5e-7)
            disp('KS linesearch: attempting to interpolate to optimal t. [Max t maintaining monotonicity criterion.]')
            fprintf(outf,'KS linesearch: attempting to interpolate to optimal t. [Max t maintaining monotonicity criterion.]\n')
            T = [LSHist((end-1):end,1)];
            SPV = [LSHist((end-1):end,2)];
            % f(t) = ( (t2 - t) f(t1) + (t - t1) f(t2) ) / (t2-t1) = 0
            % t2 f(t1) - t1 f(t2) + t (f(t2) - f(t1)) = 0
            % t = (t1 f(t2) - t2 f(t1)) / (f(t2) - f(t1))
            t_opt = (T(1)*SPV(2) - T(2)*SPV(1)) / (SPV(2) - SPV(1));
            t = t_opt;
            disp(sprintf('KS linesearch: testing monotonicity for t = %f [interp.]',t));
            fprintf(outf,'KS linesearch: testing monotonicity for t = %f [interp.]\n',t);
                
            sigma_next = sigma + t * (sigma_prime - sigma);
            kp_next = kp + t * (kp_prime - kp);
        
            % gradient of regularized F1
            [Uopt,Aopt,OptHist] = lieb_opt(sigma_next,kp_next,R,m_e,Wee,mu_reg,[]);
            F1reg = OptHist(end).lieb_fval;
            fprintf(outf,'KS linesearch: F1reg = %f, Egap = %f\n',F1reg,OptHist(end).Egap);
            
            % test that t is small is enough to yield decrease in energy
            sp = (Uext - Uopt)'*(sigma_prime - sigma);
            sp = sp + (Aext - Aopt)'*(kp_prime - kp);
            disp(sprintf('KS linesearch: monotonicity test result is sp = %f',sp));
            fprintf(outf,'KS linesearch: monotonicity test result is sp = %f\n',sp);
            if sp > 0
                % interpolation failed to yield a negative sp,
                % try again
                disp('KS linesearch: attempting to interpolate to optimal t. 2nd try.')
                fprintf(outf,'KS linesearch: attempting to interpolate to optimal t. 2nd try.\n')
                T = [T(2), t_opt];
                SPV = [SPV(2), sp];
                t_opt = (T(1)*SPV(2) - T(2)*SPV(1)) / (SPV(2) - SPV(1));
                t = t_opt;
                disp(sprintf('KS linesearch: testing monotonicity for t = %f [interp. 2nd]',t));
                fprintf(outf,'KS linesearch: testing monotonicity for t = %f [interp. 2nd]\n',t);
                
                sigma_next = sigma + t * (sigma_prime - sigma);
                kp_next = kp + t * (kp_prime - kp);
        
                % gradient of regularized F1
                [Uopt,Aopt,OptHist] = lieb_opt(sigma_next,kp_next,R,m_e,Wee,mu_reg,[]);
                F1reg = OptHist(end).lieb_fval;
                fprintf(outf,'KS linesearch: F1reg = %f, Egap = %f\n',F1reg,OptHist(end).Egap);
            
                % test that t is small is enough to yield decrease in energy
                sp = (Uext - Uopt)'*(sigma_prime - sigma);
                sp = sp + (Aext - Aopt)'*(kp_prime - kp);
                disp(sprintf('KS linesearch: monotonicity test result is sp = %f',sp));
                fprintf(outf,'KS linesearch: monotonicity test result is sp = %f\n',sp);
                if sp > 0
                    fprintf(outf,'KS linesearch debug info: tvec=[%f,%f], spv=[%f,%f]\n',T(1),T(2),SPV(1),SPV(2));
                end
            end
        end
        
        disp(sprintf('KS algorithm interpolation parameter: t = %f, sp = %f',t,sp));
        fprintf(outf,'KS algorithm interpolation parameter: t = %f, sp = %f\n',t,sp);
        
        % debug info
        Egap = OptHist(end).Egap;
        lieb_dens_err = norm([OptHist(end).grad_U; OptHist(end).grad_A]);
        fprintf(outf,'  Lieb optimization density error: ||K_ref - K_act|| = %f\n',lieb_dens_err);
        
        % update density
        sigma = sigma_next;
        kp = kp_next;
        if (lieb_dens_err > 5e-3) & (Egap > 2e-3)
            % Lieb optimization failed to reproduce density;
            % use the actual density rather than the reference density
            sigma_act = OptHist(end).grad_U / hR + sigma_next;
            kp_act = OptHist(end).grad_A / hR + kp_next;

            sigma = sigma_act;
            kp = kp_act;
            
            % print additional info
            fprintf(outf,'  F1(K_act) = %f\n',OptHist(end).lieb_fval)
            pair_Kact_aext = hR*sum(Uext.*sigma_act + Aext.*kp_act);
            pair_Knext_aext = hR*sum(Uext.*sigma_next + Aext.*kp_next);
            G1_act = OptHist(end).lieb_fval + pair_Kact_aext;
            fprintf(outf,'  G1(K_act,a_trial) = F1(K_act) + (K_act|a_trial) = %f\n',G1_act);
            fprintf(outf,'  Q1 = %f\n',G1_act+pair_Knext_aext);
            fprintf(outf,'  (K_act|a_ext) = %f\n',pair_Kact_aext);
            fprintf(outf,'  (K_next|a_ext) = %f\n',pair_Knext_aext);
        end
        
        % update KS potentials
        [Utrial,Atrial,OptHist] = lieb_opt(sigma,kp,R,m_e,[],mu_reg,[]);
        disp(sprintf('KS iteration %d: Egap(KS) = %f',it,OptHist(end).Egap));
        fprintf(outf,'KS iteration %d: Egap(KS) = %f\n',it,OptHist(end).Egap);
    end
    
    % close output file
    fclose(outf);
    
    % stop logging MATLAB output
    diary off;
end

function example_E1_vs_E0()
    % model parameters
    R = 1;
    m_e = 1;
    Ngrid = 30;
    hR = 2*pi*R/Ngrid;
    Theta = linspace(0, 2*pi*(1-1/Ngrid), Ngrid)';
    [TH1m, TH2m] = meshgrid(Theta, Theta);
    Wee = 3*sqrt(1 + cos(TH1m - TH2m));

    % Moreau-Yosida regularization parameter
    mu_reg = 0.1;    

   
    %%%%%
    Vext = cos(Theta); %cos(2*Theta) + 0.2*cos(Theta);
    Aext = 0.6 + 0*sin(Theta);
    Uext = Vext + Aext.^2 / (2*m_e);
    [Psi1,rho1,jp1,E1unreg,Egap1] = gs_2el_iter(Vext,Aext,Wee,R,m_e);
    disp(sprintf('Reference energy (unregularized): E1unreg(Uext,Aext) = %f',E1unreg));
    E1reg = E1unreg - 0.5*mu_reg*hR*sum(Uext.^2+Aext.^2);
    disp(sprintf('Reference energy (regularized): E1reg(Uext,Aext) = %f',E1reg));
    
    sigma = rho1 - mu_reg * Uext;
    kp = jp1 - mu_reg * Aext;
        

    % Kohn-Sham quantities
    [Us,As,OptHist] = lieb_opt(sigma,kp,R,m_e,[],mu_reg,[]);
    Vs = Us - As.^2 / (2*m_e);
    sigma_s = OptHist(end).grad_U / hR + sigma;
    rho_s = sigma_s + mu_reg * Us;
    kp_s = OptHist(end).grad_A / hR + kp;
    jp_s = kp_s + mu_reg * Us;

    set(0,'defaultAxesFontSize',15);
    figure(1); clf; hold on;
    pcolor(Theta,Theta,abs(Psi1).^2);
    axis tight; axis equal;
    xlabel('Particle 1');
    ylabel('Particle 2');
    title('Interacting ground state of H_1(Vext, Aext)');
    
    ThetaPer = [Theta;2*pi];
    figure(2); clf; hold on;
    title('External and KS potentials');
    subplot(2,1,1); hold on;
    plot(ThetaPer, [Vext;Vext(1)], 'b--');
    plot(ThetaPer, [Uext;Uext(1)], 'b-');
    plot(ThetaPer, [Vs;Vs(1)], 'r:');
    plot(ThetaPer, [Us;Us(1)], 'r-.');
    axis tight;    
    %xlabel('\theta');
    legend('Vext','Uext','Vs','Us');
    subplot(2,1,2); hold on;
    plot(ThetaPer, [Aext;Aext(1)], 'b-');
    plot(ThetaPer, [As;As(1)], 'r-.');
    axis tight;
    axis(axis() + [0 0 0 0.02]);
    xlabel('\theta');
    legend('Aext','As');

    figure(3); clf; hold on;
    title('External and KS potentials');
    subplot(2,1,1); hold on;
    plot(ThetaPer, [rho1; rho1(1)], 'b-');
    plot(ThetaPer, [sigma; sigma(1)], 'bo');
    plot(ThetaPer, [rho_s; rho_s(1)], 'r-.');
    plot(ThetaPer, [sigma_s; sigma_s(1)], 'rx');
    legend('\rho (int)','\sigma (int)','\rho_s (KS)','\sigma_s (KS)');
    axis tight;    
    %xlabel('\theta');
    subplot(2,1,2); hold on;
    plot(ThetaPer, [jp1; jp1(1)], 'b-');
    plot(ThetaPer, [kp; kp(1)], 'bo');
    plot(ThetaPer, [jp_s; jp_s(1)], 'r-.');
    plot(ThetaPer, [kp_s; kp_s(1)], 'rx');
    axis tight;
    xlabel('\theta');
    legend('j (int)','k (int)','js (KS)','ks (KS)');

    
    % E1 and E0 as functions of gradient steps
    STEPS = [ -0.5, -0.3, -0.2, -0.1, -0.05, -0.01, 0, 0.01, 0.02, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5];
    grad_U = hR * sigma;
    grad_A = hR * kp;
    PD = [];
    for k = 1:length(STEPS)
        zeta = STEPS(k);
        
        Ue = Uext + zeta*grad_U;
        Ae = Aext + zeta*grad_A;
        Ve = Ue - Ae.^2 / (2*m_e);
        [Psi_e,rho_e,jp_e,E1unreg,E1gap] = gs_2el_iter(Ve,Ae,Wee,R,m_e);        
        E1reg = E1unreg - 0.5*mu_reg*hR*sum(Ue.^2 + Ae.^2);
        
        Ue = Us + zeta*grad_U;
        Ae = As + zeta*grad_A;
        Ve = Ue - Ae.^2 / (2*m_e);
        [Psi_e,rho_e,jp_e,E0unreg,E0gap] = gs_1el(Ve,Ae,R,m_e);        
        E0reg = E0unreg - 0.5*mu_reg*hR*sum(Ue.^2 + Ae.^2);
        
        PD = [PD; zeta, E1reg, E1unreg, E0reg, E0unreg, E0gap];
    end

    I = find(PD(:,1) == 0);
    Eshift = PD(I,4) - PD(I,2);
    
    figure(4); clf; hold on;
    %plot(PD(:,1), PD(:,2) + Eshift, 'b:');
    %plot(PD(:,1), PD(:,4), 'r.-');
    plot(PD(:,1), PD(:,5), 'r--');
    plot(PD(:,1), PD(:,5)+PD(:,6), 'r-.');
    axis tight;
    grid on;
    legend('E0 (unreg.), KS ground state','KS exc. state');
    %legend('E1 (reg)','E0 (reg.)','E0 (unreg.), KS ground state','KS exc. state');
    xlabel('Step length, \zeta');
    ylabel('Energy');
end

function example_curdep_of_F()
    R = 1;
    m_e = 1;
    Ngrid = 30;
    hR = 2*pi*R/Ngrid;
    Theta = linspace(0, 2*pi*(1-1/Ngrid), Ngrid)';
    [TH1m, TH2m] = meshgrid(Theta, Theta);
    
    % electron-electron repulsion
    Wee = 3*sqrt(1 + cos(TH1m - TH2m));

    % log output in file
    fbas = sprintf('curdepF_%dpnts_lowf',Ngrid);
    filename = sprintf('%s.log',fbas);
    if exist(filename,'file')
        delete filename;
    end
    diary(filename);
    date
    
    % construct reference density and two reference current densities
    Vref = cos(Theta);
    Aref = 0.6 + 0*sin(Theta);
    [Psi_ref,rho_ref,jp_ref1,E_ref,Egap] = gs_2el_iter(Vref,Aref,Wee,R,m_e);
    jp_ref2 = 0*jp_ref1;

    if 1
        figure(57); clf; hold on;
        plot(Theta,jp_ref1./rho_ref,'r--');
        axis tight;
        xlabel('\theta');
        ylabel('ref. velocity');
    
        figure(58); clf; hold on;
        plot(Theta,rho_ref,'b-');
        plot(Theta,jp_ref1,'r-.');
        plot(Theta,jp_ref2,'r--');
        axis tight;
        legend('rho','jp1','jp2');
        xlabel('\theta');
        ylabel('ref. density');
        saveas(gca, strcat(fbas,'_refdens'), 'eps');
    
        figure(59); clf; hold on;
        plot(Theta,Vref,'b--');
        plot(Theta,Aref,'r--');
        axis tight;
        legend('Vref','Aref');
        xlabel('\theta');
        ylabel('ref. potential');
        saveas(gca, strcat(fbas,'_refpot'), 'eps');
    end
    
    ZETAINT = linspace(0,1,6)
    MUINT = [0.1, 0]
    PD = [];
    ITCOUNT_KS = zeros(length(ZETAINT), length(MUINT));
    ITCOUNT_INT = zeros(length(ZETAINT), length(MUINT));
    for j = 1:length(ZETAINT)
        zeta = ZETAINT(j);
        
        % reference current density
        jp_ref = zeta*jp_ref2 + (1-zeta)*jp_ref1;
        m_ref = R*hR*sum(jp_ref./rho_ref) / (2*pi);        

        UAinit1 = [];
        UAinit0 = [];
        for l = 1:length(MUINT)
            % regularization parameter
            mu_reg = MUINT(l);
            
            % Lieb optimization to determine F1(rho_ref,jp_ref)
            [U_ext, A_ext, OptHist] = lieb_opt(rho_ref,jp_ref,R,m_e,Wee,mu_reg,UAinit1);
            F1 = OptHist(end).lieb_fval;
            Egap_int = OptHist(end).Egap;
            grad_norm1 = norm([OptHist(end).grad_U; OptHist(end).grad_A]);
            ITCOUNT_INT(j,l) = length(OptHist);
            UAinit1 = [U_ext, A_ext];            

            % Lieb optimization to determine F0(rho_ref,jp_ref)
            [U_KS, A_KS, OptHist] = lieb_opt(rho_ref, jp_ref,R,m_e,[],mu_reg,UAinit0);
            F0 = OptHist(end).lieb_fval;
            Egap_KS = OptHist(end).Egap;
            grad_norm0 = norm([OptHist(end).grad_U; OptHist(end).grad_A]);
            ITCOUNT_KS(j,l) = length(OptHist);
            UAinit0 = [U_KS, A_KS];
        
            % save plot data
            PD = [PD ; zeta, l, F0, F1, grad_norm0, grad_norm1, m_ref, Egap_KS, Egap_int];
        end
    end

    PD
    
    tabtxt = 'Iteration count in Lieb opt.';
    for l = 1:length(MUINT)
        tabtxt = strcat(tabtxt,sprintf('\nmu=%f: ',MUINT(l)));
        for j = 1:length(ZETAINT)
            tabtxt = strcat(tabtxt,sprintf(' %6d',ITCOUNT_KS(j,l)));
        end
        tabtxt = strcat(tabtxt,sprintf('   [KS]\n---        : '));
        for j = 1:length(ZETAINT)
            tabtxt = strcat(tabtxt,sprintf(' %6d',ITCOUNT_INT(j,l)));
        end
    end
    disp('----------')
    disp(tabtxt);
    disp('----------')
    
    figure(60); clf; hold on;
    for l = 1:length(MUINT)
        IX = find(PD(:,2) == l);
        plot(PD(IX,1), PD(IX,3), '.-','Color',[0,0,1.0/l]);
        plot(PD(IX,1), PD(IX,4), 'o-','Color',[1.0/l,0,0]);
        if l==1
            legend('KS grad norm','Int. grad norm');
        end
    end
    legend('F0 = Ts','F1');
    xlabel('Interpolation parameter \zeta');
    ylabel('F(\rho,j_p(\zeta))');
    saveas(gca, strcat(fbas,'_F0F1'), 'eps');
    
    figure(61); clf; hold on;
    for l = 1:length(MUINT)
        IX = find(PD(:,2) == l);
        plot(PD(IX,1), PD(IX,5), '.-','Color',[0,0,1.0/l]);
        plot(PD(IX,1), PD(IX,6), 'o-','Color',[1.0/l,0,0]);
        if l==1
            legend('KS grad norm','Int. grad norm');
        end
    end
    xlabel('Interpolation parameter \zeta');
    ylabel('||gradient||');
    saveas(gca, strcat(fbas,'_gradnorm'), 'eps');
    
    figure(62); clf; hold on;
    for l = 1:length(MUINT)
        IX = find(PD(:,2) == l);
        subplot(3,1,1); hold on;
        plot(PD(IX,1), PD(IX,8), '.-','Color',[0,0,1.0/l]);
        ylabel('KS energy gap');
        subplot(3,1,2); hold on;
        plot(PD(IX,1), PD(IX,9),'o-','Color',[1.0/l,0,0]);
        ylabel('Int. energy gap');
        subplot(3,1,3); hold on;
        plot(PD(IX,1), PD(IX,7), 'kx-');
        ylabel('m of reference jp');
    end
    xlabel('Interpolation parameter \zeta');
    saveas(gca, strcat(fbas,'_m_Egap'), 'eps');
    
    % stop logging output
    diary off;
end


function example_uniform_dens()
    R = 1;
    m_e = 1;
    Ngrid = 30;
    hR = 2*pi*R/Ngrid;
    Theta = linspace(0, 2*pi*(1-1/Ngrid), Ngrid)';
    [TH1m, TH2m] = meshgrid(Theta, Theta);
    %Wee = 3*sqrt(1 + cos(TH1m - TH2m));
    Wee = [];
    
    rho_ref = 1 + 0*Theta;
    rho_ref = 2 * rho_ref / sum(rho_ref*hR);
    
    % r(n) = sqrt(rho(n))
    % j(n) = -i/(4h) rho exp(-i chi(n)) (exp(i chi(n+1)) - exp(i chi(n-1))) + c.c.
    %      = 1/(2h) rho (sin(chi(n+1)-chi(n)) - sin(chi(n)-chi(n-1)))
    %
    %
    % E(v,A) = min_m ( 1/2 (m/R + A)^2 + v)
    %        = min_m (1/(2*R^2) (m + AR)^2 + v)
    %        = 1/(2*R^2) frac(AR)^2 + v
    %
    % E'(u,A) = E(v-A^2/2,A) = = 1/(2*R^2) frac(AR)^2 + (u - A^2 / 2)

    %mu_reg = 0.55;
    MUINT = [0,0.2,0.4];
    MEFF_INT = linspace(0,6,12);
    
    F1 = zeros(length(MUINT),length(MEFF_INT));
    EGAP = 0*F1;
    GNORM = 0*F1;
    VMEAN = 0*F1;
    VSTD = 0*F1;
    AMEAN = 0*F1;
    ASTD = 0*F1;
    
    for p = 1:length(MEFF_INT)
        jp_ref =  0.3/R * rho_ref;
        meff = hR*R*sum(jp_ref./rho_ref) / (2*pi);            
        for q = 1:length(MUINT)
            mu_reg = MUINT(q);
            Uinit = 0*Theta;
            Ainit = -meff*Theta;
            [Uopt, Aopt, OptHist] = lieb_opt(rho_ref,jp_ref,R,m_e,Wee,mu_reg,[Uinit, Ainit]);
            sigma = OptHist(end).grad_U / hR + rho_ref;
            rho = sigma + mu_reg*Uopt;
            kp = OptHist(end).grad_A / hR + jp_ref;
            jp = kp + mu_reg*Aopt;
        
            Vopt = Uopt - Aopt.^2 / (2*m_e);
        
            figure(1); clf; hold on;
            subplot(2,1,1); hold on;
            plot(Theta,Uopt,'k--');
            plot(Theta,rho_ref,'k+-');
            plot(Theta,sigma,'b.-');
            axis tight;
            V = axis();
            V(3) = min([0,V(3)]); V(4) = V(4) + 0.01;
            axis(V);
            subplot(2,1,2); hold on;
            plot(Theta,Aopt,'k--');
            plot(Theta,jp_ref,'k+-');
            plot(Theta,kp,'b.-');
            axis tight;
            V = axis();
            V(3) = min([0,V(3)]); V(4) = V(4) + 0.01;
            axis(V);
            xlabel('\theta');
        
            
            VMEAN(q,p) = mean(Vopt);
            VSTD(q,p) = std(Vopt);
            AMEAN(q,p) = mean(Aopt);
            ASTD(q,p) = std(Aopt);
            F1(q,p) = OptHist(end).lieb_fval;
            EGAP(q,p) = OptHist(end).Egap;
            GNORM(q,p) = norm([OptHist(end).grad_U; OptHist(end).grad_A]);
            disp(sprintf('mu=%f, meff=%f, F1=%f, Egap=%f, gnorm=%f\n',mu_reg,meff,F1(q,p),EGAP(q,p),GNORM(q,p)))
        end
    end
    
    mu_max = max(MUINT);
    
    figure(2); clf; hold on;
    for q = 1:length(MUINT)
        t = MUINT(q)/mu_max;
        C = t*[1,0,0] + (1-t)*[0,0,1];
        plot(MEFF_INT,F1(q,:),'Color',C);
    end
    grid on;
    ylabel('F1')
    figure(3); clf; hold on;
    for q = 1:length(MUINT)
        t = MUINT(q)/mu_max;
        C = t*[1,0,0] + (1-t)*[0,0,1];
        plot(MEFF_INT,EGAP(q,:),'Color',C);
    end
    grid on;
    ylabel('Egap')
    figure(4); clf; hold on;
    for q = 1:length(MUINT)
        t = MUINT(q)/mu_max;
        C = t*[1,0,0] + (1-t)*[0,0,1];
        plot(MEFF_INT,GNORM(q,:),'Color',C);
    end
    grid on;
    ylabel('grad.norm')
    figure(5); clf; hold on;
    for q = 1:length(MUINT)
        t = MUINT(q)/mu_max;
        C = t*[1,0,0] + (1-t)*[0,0,1];
        plot(MEFF_INT,VMEAN(q,:),'.-','Color',C);
        plot(MEFF_INT,AMEAN(q,:),'x-','Color',C);
    end
    grid on;
    ylabel('potential')
    
    VSTD
    ASTD
end

function example_lieb_opt(choice)
    R = 1;
    m_e = 1;
    Ngrid = 100;
    hR = 2*pi*R/Ngrid;
    Theta = linspace(0, 2*pi*(1-1/Ngrid), Ngrid)';
    [TH1m, TH2m] = meshgrid(Theta, Theta);
    Wee = 3*sqrt(1 + cos(TH1m - TH2m));
    %Wee = 0*Wee;
    
    if choice==1
        % illustrate that a correlated wave function can, with difficulty,
        % get close to a fractional value of m = \int R jp./rho dtheta.
        rho_ref = 1.1 + cos(Theta);
        rho_ref = 2 * rho_ref / sum(rho_ref*hR);
        jp_ref = 0.25/pi * rho_ref;
    elseif choice==2
        % illustrate that uncorrelated wave functions cannot reproduce
        % a fractional value of m = \int R jp./rho dtheta...
        % unless we regularize and reinterpret rho,jp as carrier densities
        rho_ref = 1.1 + cos(Theta);
        rho_ref = 2 * rho_ref / sum(rho_ref*hR);
        jp_ref = 0.25/pi * rho_ref;
        % set electron-electron repulsion to zero
        Wee = [];
    else
        % illustrate easy case where the reference density is the ground
        % state of a simple potential
        Vref = cos(2*Theta);
        Aref = 0.5 + sin(Theta);
        [Psi,rho_ref,jp_ref,E_ref,Egap] = gs_2el_iter(Vref,Aref,Wee,R,m_e);
    end
    m_ref = sum(hR*jp_ref./rho_ref)
    mu_reg = 0.05;
    [Uopt, Aopt, OptHist] = lieb_opt(rho_ref, jp_ref,R,m_e,Wee,mu_reg,[]);
end

function example_lieb_grad_test()
    R = 1;
    mass = 1;    
    Ngrid = 60;
    hR = 2*pi*R/Ngrid;
    Theta = linspace(0, 2*pi*(1-1/Ngrid), Ngrid)';
    [TH1m, TH2m] = meshgrid(Theta, Theta);
    %Wee = [];
    Wee = 3*sqrt(1 + cos(TH1m - TH2m));
    
    %rho_ref = 1.1 + cos(Theta);
    %rho_ref = 2 * rho_ref / sum(rho_ref*hR);
    %jp_ref = 0.25/pi * rho_ref;
    rho_ref = 0*Theta;
    jp_ref = 0*Theta;
    
    mu_ref = 0.0;
    
    U = cos(Theta);
    A = sin(2*Theta) + cos(3*Theta);
    %U = rand(Ngrid,1);
    %A = rand(Ngrid,1);
    
    lieb_grad_test(U,A,rho_ref,jp_ref,R,mass,Wee,mu_ref);
end

function example_gs_calcs(choice)
    % model parameters
    R = 1.5;
    m_e = 1;
    
    Ngrid = 100;
    Theta = linspace(0, 2*pi*(1-1/Ngrid), Ngrid)';    
    [TH1m, TH2m] = meshgrid(Theta, Theta);
    if choice == 1
        V = 2*cos(2*Theta);
        A = 0.6 + 0.3*cos(Theta);
        Wee = 0*TH1m;
        disp('Example run: choice 1, no electron-electron repulsion.')
    elseif choice == 2
        %Wee = 1 ./ (R*sqrt(1e-3 + (1 - cos(TH1m - TH2m)).^2 + sin(TH1m - TH2m).^2));
        V = 0*Theta;
        A = 0.6 + 0.3*cos(4*Theta);
        Wee = 3*sqrt(1 + cos(TH1m - TH2m));
        disp('Example run: choice 2, electron-electron repulsion, constant V.')
    else
        %Wee = 1 ./ (R*sqrt(1e-3 + (1 - cos(TH1m - TH2m)).^2 + sin(TH1m - TH2m).^2));
        V = 2*cos(3*Theta) + cos(Theta);
        A = 0.6 + 0.3*cos(4*Theta);
        Wee = 3*sqrt(1 + cos(TH1m - TH2m));
        disp('Example run: choice 3, electron-electron repulsion.')
    end
    
    % solve single-orbital problem
    [Psi0, rho0, jp0, E0, Egap0] = gs_1el(V,A,R,m_e);    
    
    set(0,'defaultAxesFontSize',15);
    figure(1); clf; hold on;
    plot(Theta, real(Psi0), 'b-');
    plot(Theta, imag(Psi0), 'r-');
    plot(Theta, V, 'k--');
    plot(Theta, A, 'm--');
    axis tight;
    legend('Re(\phi)','Im(\phi)','V','A');

    figure(2); clf; hold on;
    plot(Theta, rho0, 'k-');
    plot(Theta, jp0, 'b-');
    plot(Theta, V, 'k--');
    plot(Theta, A, 'm--');
    axis tight;
    legend('\rho[\phi]','j_p[\phi]','V','A');

    m0 = sum(R*jp0./rho0) / Ngrid;
    disp(sprintf('A one-orbital system has quantized integral over R j_p/rho:\n  m0 = %f',m0))
    
    % solve correlated two-electron problem (with zero electron-electron repulsion)
    [Psi1,rho1,jp1,E1,Egap1] = gs_2el_iter(V,A,Wee,R,m_e);
    
    figure(3); clf; hold on;
    plot(Theta, rho1, 'k-');
    plot(Theta, rho0, 'm--');
    axis tight;
    legend('Int.','KS (diag.)');
    title('Corr. vs. single-orb. density');

    figure(4); clf; hold on;
    plot(Theta, jp1, 'k-');
    plot(Theta, jp0, 'm--');
    axis tight;
    legend('Int.','KS (diag.)');
    title('Corr. vs. single-orb. current density');

    m_int = sum(R*jp1./rho1) / Ngrid
    
    int_vs_KS = [
        E1, E0, E1/E0, E1/E0-2 ;
        Egap1, Egap0, Egap1/Egap0, Egap1/Egap0-2 ;
    ]
    
    figure(5); clf; hold on;
    pcolor(Theta,Theta,abs(Psi1).^2);
    axis tight; axis equal;
    shading interp;
    xlabel('particle 1');
    ylabel('particle 2');
    title('corr. ground state');   
end

function Res = lieb_fun_eval(U, A, rho_ref, jp_ref, R, mass, Wee, mu_reg)
    % This function evaluates the "Lieb functional"
    %
    %  G(U,A,rho_ref,jp_ref) = E(V,A) - (rho_ref|U) - (jp_ref|A)
    %                          - mu/2 ||u||^2 - mu/2 ||A||^2
    %
    % where V = U - A.^2/(2*mass). Note that the input is U, which
    % already contains the diamagnetic term.
    % Also computed are the gradients of G w.r.t. U and A, which
    % are simply rho - rho_ref and jp - jp_ref corrected by a term
    % proportional to the regularization parameter.
    Ngrid = length(rho_ref);
    hR = 2*pi*R/Ngrid;
    V = U - A.^2 / (2*mass);
    
    if numel(Wee) == 0
        disp('INFO: Lieb eval for uncorrelated system with W=0.')
        % no electron repulsion operator supplied,
        % solve as one-particle problem
        [Psi,rho,jp,E,Egap] = gs_1el(V,A,R,mass);
        perm_sym = +1;
    else
        % solve for correlated two-particle wave function
        [Psi,rho,jp,E,Egap] = gs_2el_iter(V,A,Wee,R,mass);
        nfac = sum(sum(conj(Psi) .* Psi));
        perm_sym = sum(sum(conj(Psi.') .* Psi)) / nfac;
    end

    % setup data structure for return value
    Res = struct();

    % function value
    dGreg = 0.5 * mu_reg * sum(U.^2 + A.^2)*hR;
    G = E - sum(rho_ref.*U)*hR - sum(jp_ref.*A)*hR - dGreg;
    Res.lieb_fval = G;
    
    % correct gradients by regularization term
    Res.grad_U = hR * (rho - rho_ref - mu_reg*U);
    Res.grad_A = hR * (jp - jp_ref - mu_reg*A);

    % auxilary information
    Res.U = U;
    Res.A = A;
    Res.energy = E;
    Res.Egap = Egap;
    Res.perm_sym = perm_sym;
end

function lieb_grad_test(U,A,rho_ref,jp_ref,R,mass,Wee,mu_reg)
    Ngrid = length(U);
    hR = 2*pi*R/Ngrid;
    Theta = linspace(0, 2*pi*(1-1/Ngrid), Ngrid)';

    Res0 = lieb_fun_eval(U, A, rho_ref, jp_ref, R, mass, Wee, mu_reg);
    
    FDstep = 0.01;
    
    figure(1); clf; hold on;
    V = U - A.^2/(2*mass);
    [Psi,rho,jp,E,Egap] = gs_2el_iter(V,A,Wee,R,mass);
    plot(Theta,U,'k-');
    plot(Theta,A,'k--');
    plot(Theta,(Res0.grad_U+rho_ref)/hR,'b-');
    plot(Theta,(Res0.grad_A+jp_ref)/hR,'r-');
    plot(Theta,rho,'b--');
    plot(Theta,jp,'r--');
    axis tight;
    grid on;
    
    figure(2); clf; hold on;
    pcolor(Theta,Theta,abs(Psi).^2);
    axis tight;
    
    
    GradComp = [];
    for k = 1:5:Ngrid
        % gradient w.r.t. U
        Upos = U;
        Upos(k) = Upos(k) + FDstep;
        ResUforw = lieb_fun_eval(Upos, A, rho_ref, jp_ref, R, mass, Wee, mu_reg);
        Uneg = U;
        Uneg(k) = Uneg(k) - FDstep;
        ResUback = lieb_fun_eval(Uneg, A, rho_ref, jp_ref, R, mass, Wee, mu_reg);
    
        FDgrad_U_k = (ResUforw.lieb_fval - ResUback.lieb_fval) / (2*FDstep);
        gradU_k = Res0.grad_U(k);
    
        disp(sprintf('Pnt %d: Eforw = %.10g, E0 = %.10g, Eback = %0.10g',k,ResUforw.lieb_fval,Res0.lieb_fval,ResUback.lieb_fval))
        
        % gradient w.r.t. A
        Apos = A;
        Apos(k) = Apos(k) + FDstep;
        ResAforw = lieb_fun_eval(U, Apos, rho_ref, jp_ref, R, mass, Wee, mu_reg);
        Aneg = A;
        Aneg(k) = Aneg(k) - FDstep;
        ResAback = lieb_fun_eval(U, Aneg, rho_ref, jp_ref, R, mass, Wee, mu_reg);

        FDgrad_A_k = (ResAforw.lieb_fval - ResAback.lieb_fval) / (2*FDstep);
        gradA_k = Res0.grad_A(k);
        
        % save components
        GradComp = [GradComp ; k gradU_k FDgrad_U_k, gradA_k, FDgrad_A_k];
    end
    
    for r = 1:size(GradComp,1)
        disp(sprintf('Grid point %d: (%f,%f)_anl ~ (%f,%f)_FF',GradComp(r,1),GradComp(r,2),GradComp(r,4),GradComp(r,3),GradComp(r,5)))
        %GradComp
    end
    disp(sprintf('Overall: err_U = %f, err_A = %f',norm(GradComp(:,2)-GradComp(:,3)),norm(GradComp(:,4)-GradComp(:,5))))
end

function [U,A,Fun_bound] = lieb_bundle_step(OptHist)
    Ndim = length(OptHist);
    Ngrid = length(OptHist(1).U);
    
    trust_radius = 0.5; % * Ngrid;
    
    Fun_X = zeros(1,Ndim);
    Grad_X = zeros(2*Ngrid, Ndim);
    X = zeros(2*Ngrid, Ndim);
    S = zeros(2*Ngrid, Ndim);
    for k = 1:Ndim
        % extrapolate P(x) = fk + gk^T (x - x_k)
        Fun_X(k) = OptHist(k).lieb_fval;
        Grad_X(:,k) = [OptHist(k).grad_U; OptHist(k).grad_A];
        X(:,k) = [OptHist(k).U; OptHist(k).A];
        
        % determine step to minimum
        S(:,k) = Grad_X(:,k); % / Mcurv;
        max_norm_Sk = max(abs(S(:,k)));
        S(:,k) = S(:,k) * trust_radius / max_norm_Sk;
    end
    
    Fbest = min(Fun_X) - 1.0e50;
    Ybest = 0*X(:,1);
    [Dummy, Kmax] = max(Fun_X);
    Kstart = max([Kmax-10,1]);
    Kend = length(Fun_X);
    for k = Kstart:Kend   %Kmax:Kmax   %1:Ndim
        sfac_min = 0;
        sfac = 1;
        sfac_max = 1;
        not_conv = true;
        loop_cnt = 0;
        while not_conv & (loop_cnt < 30)
            loop_cnt = loop_cnt + 1;
            % best point for function #k
            Y = X(:,k) + sfac * S(:,k);
        
            % evaluate all functions at Y, omitting quadratic term
            %Z = repmat(Y,[1,Ndim]) - X;
            %Plane_Y = Fun_X + sum(Grad_X.*Z, 1);
            Plane_Y = 0*Fun_X;
            for j = 1:Ndim
                Plane_Y(j) = Fun_X(j) + Grad_X(:,j)'*(Y - X(:,j));
            end
        
            % update bounds
            Fpred_Y = min(Plane_Y);
            if Fpred_Y < Plane_Y(k)
                sfac_max = sfac;
            else
                sfac_min = sfac;
            end
            sfac = 0.8*sfac_min + 0.2*sfac_max;
                        
            % stopping criterion
            if sfac_max - sfac_min < 1.0e-7
                not_conv = false;
            end
        end
        
        % remember best point encountered so far
        if Fpred_Y > Fbest
            %disp(sprintf('INFO: Fbest increased from %g to %g in iteration %d',Fbest,Fpred_Y,k))
            Fbest = Fpred_Y;
            Ybest = Y;
        end

        if 0 %Fpred_Y < Fun_X(k) - 5.0e-6
            disp(sprintf('********\ndebug output:'))
            disp(sprintf('  k = %d',k))
            disp(sprintf('sfac_min = %f, sfac_max = %f, diff = %f',sfac_min,sfac_max,sfac_max-sfac_min))
            disp(sprintf('    F(y) = %f, discr = %f',Fpred_Y,Plane_Y(k)-Fpred_Y))
            for l = 1:Ndim
                if l==k
                    arr = '--> ';
                else
                    arr = '    ';
                end
                Tmp = Grad_X(:,l)'*(X(:,k) - X(:,l));
                disp(sprintf('%sF(x%d) = %f,  Plane(y) = %f, gt = %f',arr,l,Fun_X(l),Plane_Y(l),Tmp))
            end
            error('bundle step prediction below sampled function value')
        end
    end
    
    % return values
    U = Ybest(1:Ngrid);
    A = Ybest((Ngrid+1):end);
    Fun_bound = Fbest;
end

function [Res, best_step] = lieb_opt_ls(InitPnt,rho_ref,jp_ref,R,mass,Wee,mu_reg)
    % try a step of length 1
    step = 1;    
    Unew = InitPnt.U + step*InitPnt.search_dir_U;
    Anew = InitPnt.A + step*InitPnt.search_dir_A;
    NewPnt = lieb_fun_eval(Unew, Anew, rho_ref, jp_ref, R, mass, Wee, mu_reg);
        
    MicroHist = [0, InitPnt.lieb_fval; step, NewPnt.lieb_fval];        
    if NewPnt.lieb_fval > InitPnt.lieb_fval
        % step too conservative, try longer steps
        BestPnt = InitPnt;
        while (NewPnt.lieb_fval > BestPnt.lieb_fval) & (step < 10)
            % save data for best step so far
            BestPnt = NewPnt;
            best_step = step;
            
            % try longer step
            step = 2 * step;
            Unew = InitPnt.U + step*InitPnt.search_dir_U;
            Anew = InitPnt.A + step*InitPnt.search_dir_A;
            NewPnt = lieb_fun_eval(Unew, Anew, rho_ref, jp_ref, R, mass, Wee, mu_reg);
            % update microiteration history
            MicroHist = [MicroHist ; step, NewPnt.lieb_fval];
        end
        
        if size(MicroHist,1) > 2
            NMH = size(MicroHist,1);
            IX = [1,NMH-1,NMH];
            Cfit = polyfit(MicroHist(IX,1),MicroHist(IX,2),2);
            step = -Cfit(2) / (2*Cfit(1));
            if abs(step) > 10.0
                step = MicroHist(end-1,1);
            end
            Unew = InitPnt.U + step*InitPnt.search_dir_U;
            Anew = InitPnt.A + step*InitPnt.search_dir_A;
            NewPnt = lieb_fun_eval(Unew, Anew, rho_ref, jp_ref, R, mass, Wee, mu_reg);
            if NewPnt.lieb_fval < InitPnt.lieb_fval - 1.0e-6
                disp('WARNING: Step length extrapolation failed.');
            end
            if NewPnt.lieb_fval > BestPnt.lieb_fval
                BestPnt = NewPnt;
                best_step = step;
            end
        end
    else
        % step too long, try shorter steps
        while (NewPnt.lieb_fval < InitPnt.lieb_fval) & (step > 0.02)
            step = 0.5 * step;
            Unew = InitPnt.U + step*InitPnt.search_dir_U;
            Anew = InitPnt.A + step*InitPnt.search_dir_A;
            NewPnt = lieb_fun_eval(Unew, Anew, rho_ref, jp_ref, R, mass, Wee, mu_reg);
            % update microiteration history
            MicroHist = [MicroHist ; step, NewPnt.lieb_fval];
        end
        
        if size(MicroHist,1) > 2
            NMH = size(MicroHist,1);
            IX = [1,NMH-1,NMH];
            Cfit = polyfit(MicroHist(IX,1),MicroHist(IX,2),2);
            step = -Cfit(2) / (2*Cfit(1));
            if abs(step) > 10.0
                step = 0.5 * MicroHist(end,1);
            end
            Unew = InitPnt.U + step*InitPnt.search_dir_U;
            Anew = InitPnt.A + step*InitPnt.search_dir_A;
            NewPnt = lieb_fun_eval(Unew, Anew, rho_ref, jp_ref, R, mass, Wee, mu_reg);
            %figure(60+it); clf; hold on;
            %STMP = linspace(0,1,100);                
            %plot(STMP, polyval(Cfit,STMP), 'r-');
            %plot(MicroHist(:,1), MicroHist(:,2), 'ko');
            if NewPnt.lieb_fval < InitPnt.lieb_fval - 1.0e-6
                disp('WARNING: Step length interpolation failed.');
            end
        end
        
        BestPnt = NewPnt;
        best_step = step;
    end
    
    % return value
    Res = BestPnt;
end

function lieb_concavity_check(OptHist,rho_ref,jp_ref,R,mass,Wee,mu_reg)
    Ndim = length(OptHist);
    Ngrid = length(OptHist(1).U);
    hR = 2*pi*R/Ngrid;
    
    Fun_X = zeros(1,Ndim);
    Grad_X = zeros(2*Ngrid, Ndim);
    X = zeros(2*Ngrid, Ndim);
    for k = 1:Ndim
        % extrapolate P(x) = fk + gk^T (x - x_k)
        Fun_X(k) = OptHist(k).lieb_fval;
        Grad_X(:,k) = [OptHist(k).grad_U; OptHist(k).grad_A];
        X(:,k) = [OptHist(k).U; OptHist(k).A];
    end
    
    % check concavity
    worst_diff = 0;
    worst_kl = [];
    for k = 1:Ndim
        for l = 1:Ndim
            Pl = Fun_X(l) + Grad_X(:,l)'*(X(:,k) - X(:,l));
            if Pl < Fun_X(k)
                %disp(sprintf('Failed concavity check! F(x%d) = %f, P%d(x%d) = %f, diff = %f',k,Fun_X(k),l,k,Pl,Fun_X(k)-Pl))
                if worst_diff < Fun_X(k) - Pl
                    worst_diff = Fun_X(k) - Pl;
                    worst_kl = [k,l];
                end
            end
        end
    end
    
    if worst_diff > 0.01
        k = worst_kl(1);
        l = worst_kl(2);
        
        disp(sprintf('\n-*-*-*-*-*-*-\nERROR: Nonconcavity for k=%d, l=%d, Ndim=%d, diff=%f',k,l,Ndim,worst_diff))
        disp(sprintf('Permutation symmetry:\n Psi%d has %f',k,OptHist(k).perm_sym))
        disp(sprintf(' Psi%d has %f',l,OptHist(l).perm_sym))
        
        % make plot to illustrate non-concavity
        ZETA = [-0.1, linspace(0,1,20), 1.1];
        GINTERP = 0*ZETA;
        GALT = 0*ZETA;
        for j = 1:length(ZETA)
            U = (1-ZETA(j))*OptHist(k).U + ZETA(j)*OptHist(l).U;
            A = (1-ZETA(j))*OptHist(k).A + ZETA(j)*OptHist(l).A;
            InterpPnt = lieb_fun_eval(U, A, rho_ref, jp_ref, R, mass, Wee, mu_reg);
            GINTERP(j) = InterpPnt.lieb_fval;
            
            % also try iterative solver
            V = U - A.^2 / (2*mass);
            [Psi,rho,jp,E,Egap] = gs_2el_iter(V,A,Wee,R,mass);
            dGreg = 0.5 * mu_reg * sum(U.^2 + A.^2)*hR;
            G = E - sum(rho_ref.*U)*hR - sum(jp_ref.*A)*hR - dGreg;
            GALT(j) = G;
            if abs(ZETA(j)) < 1.0e-7
                disp(sprintf('Gap at ak = (Uk,Ak): %f',Egap))
                gradU1 = hR * (rho - rho_ref - mu_reg*U);
                gradA1 = hR * (jp - jp_ref - mu_reg*A);
                sl_alt_k = gradU1' * (OptHist(l).U - OptHist(k).U);
                sl_alt_k = sl_alt_k + gradA1' * (OptHist(l).A - OptHist(k).A);
                disp('Testing Lieb gradients at ak = (Uk,Ak).')
                lieb_grad_test(U,A,rho_ref,jp_ref,R,mass,Wee,mu_reg)
            elseif abs(ZETA(j)-1) < 1.0e-7
                disp(sprintf('Gap at al = (Ul,Al): %f',Egap))
                gradU2 = hR * (rho - rho_ref - mu_reg*U);
                gradA2 = hR * (jp - jp_ref - mu_reg*A);
                sl_alt_l = gradU2' * (OptHist(k).U - OptHist(l).U);
                sl_alt_l = sl_alt_l + gradA2' * (OptHist(k).A - OptHist(l).A);
                disp('Testing Lieb gradients at al = (Ul,Al).')
                lieb_grad_test(U,A,rho_ref,jp_ref,R,mass,Wee,mu_reg)
            end
        end

        figure(100); clf; hold on;
        subplot(2,1,1); hold on;
        plot(OptHist(k).U,'b-');
        plot(OptHist(l).U,'r-');
        legend('U1','U2');
        ylabel('U');
        subplot(2,1,2); hold on;
        plot(OptHist(k).A,'b-');
        plot(OptHist(l).A,'r-');
        legend('A1','A2');
        xlabel('\theta');
        ylabel('A');

        figure(101); clf; hold on;
        plot(ZETA, GALT, 'bv:');
        plot(ZETA, GINTERP, 'k.-');
        slope_k = OptHist(k).grad_U'*(OptHist(l).U - OptHist(k).U);
        slope_k = slope_k + OptHist(k).grad_A'*(OptHist(l).A - OptHist(k).A);
        slope_l = OptHist(l).grad_U'*(OptHist(k).U - OptHist(l).U);
        slope_l = slope_l + OptHist(l).grad_A'*(OptHist(k).A - OptHist(l).A);
        plot(ZETA, OptHist(k).lieb_fval + slope_k*ZETA, 'g-');
        plot(ZETA, OptHist(k).lieb_fval + sl_alt_k*ZETA, 'g--');
        plot(ZETA, OptHist(l).lieb_fval + slope_l*(1-ZETA), 'm-');
        plot(ZETA, OptHist(l).lieb_fval + sl_alt_l*(1-ZETA), 'm--');
        axis tight;
        grid on;
        xlabel('Interpolation param. \zeta');
        ylabel('G(J_{ref}, \zeta a_1 + (1-\zeta) a_2)');
        error('Nonconcavity!')
    end
end

function [Uopt,Aopt,OptHist] = lieb_opt(rho_ref,jp_ref,R,mass,Wee,mu_reg,UAstart)
    Ngrid = length(rho_ref);
    hR = 2*pi*R/Ngrid;
    Theta = linspace(0, 2*pi*(1-1/Ngrid), Ngrid)';

    disp(sprintf('\n\n--- --- ---\nRunning Lieb optimization with parameters:'));
    disp(sprintf('  R = %f\n  mass = %f\n  mu_reg = %f',R,mass,mu_reg));
    if numel(Wee) > 0
        disp(sprintf('  electron-electron repulsion: present (interacting system), norm(Wee) = %f',norm(Wee)));
    else
        disp('  electron-electron repulsion: absent (Kohn-Sham system)');
    end
    
    show_plots = 0;
    if show_plots
        figure(22); clf; hold on;
        subplot(2,1,1); hold on;
        title('Lieb opt. trial density');
        plot(Theta,rho_ref,'k--');
        axis tight;
        subplot(2,1,2); hold on;
        title('trial current density');
        plot(Theta,jp_ref,'k--');    
        axis tight;
        figure(23); clf; hold on;
        subplot(2,1,1); hold on;
        title('Lieb opt. trial V');
        subplot(2,1,2); hold on;
        title('trial A');
    end

    % optimization parameters
    use_bundle_method = true;
    only_grad_steps = true;
    max_iter = 300;
    conv_tol = 1.0e-5 * sqrt(Ngrid/30);
    
    % initial trial potentials and related data
    if numel(UAstart) > 0
        % initial potential provided as function argument
        Utrial = UAstart(:,1);
        Atrial = UAstart(:,2);
    else
        % construct initial potential
        Utrial = 1/pi - rho_ref;
        Atrial = 1/pi - jp_ref; %0*Theta;
    end
    
    TrialPnt = lieb_fun_eval(Utrial, Atrial, rho_ref, jp_ref, R, mass, Wee, mu_reg);
    
    InvHessian = eye(2*Ngrid);
    OptHist = [];
    it = 0;
    num_decreases = 0;
    not_converged = true;
    while (it < max_iter) & not_converged
        it = it + 1;
        if show_plots
            figure(22);
            subplot(2,1,1); hold on;
            plot(Theta,drho_trial);
            subplot(2,1,2); hold on;
            plot(Theta,djp_trial);
            figure(23);        
            subplot(2,1,1); hold on;
            plot(Theta,Utrial-Atrial.^2/(2*mass));
            subplot(2,1,2); hold on;
            plot(Theta,Atrial);
        end

        rho = TrialPnt.grad_U + rho_ref;
        jp = TrialPnt.grad_A + jp_ref;
        m_trial = sum(hR*jp./rho) / (2*pi);
        m_ref = sum(hR*jp_ref./rho_ref) / (2*pi);
        m_trial_vs_ref = [m_trial, m_ref];
        
        % amend lieb_fun_eval() output with search direction
        % (sometimes overwritten below)
        TrialPnt.search_dir_U = TrialPnt.grad_U;
        TrialPnt.search_dir_A = TrialPnt.grad_A;
        
        % update approximate inverse Hessian
        if it > 10
            PrevTrialPnt = OptHist(end);
            sk = [TrialPnt.U ; TrialPnt.A] - [PrevTrialPnt.U; PrevTrialPnt.A];
            yk = [TrialPnt.grad_U; TrialPnt.grad_A] - [PrevTrialPnt.grad_U; PrevTrialPnt.grad_A];
            ydots = yk'*sk;
            Pmat_sy = eye(2*Ngrid) - sk*yk' / ydots;
            InvHessian = Pmat_sy * InvHessian * Pmat_sy' + sk*sk'/ydots;
            if ~only_grad_steps
                % override gradient as line search direction
                search_dir = InvHessian * [TrialPnt.U; TrialPnt.A];
                TrialPnt.search_dir_U = search_dir(1:Ngrid);
                TrialPnt.search_dir_A = search_dir((Ngrid+1):end);
            end
        end
        
        % update optimization history
        OptHist = [OptHist; TrialPnt];
        
        if use_bundle_method
            [Ub,Ab,Gbound] = lieb_bundle_step(OptHist);
            NextPnt = lieb_fun_eval(Ub, Ab, rho_ref, jp_ref, R, mass, Wee, mu_reg);
            disp(sprintf('Iteration %d: G = %f [-> Gnew = %f, DeltaG = %g], Gbound = %f',it,TrialPnt.lieb_fval,NextPnt.lieb_fval,NextPnt.lieb_fval-TrialPnt.lieb_fval,Gbound))
        else
            % line search along search direction
            [NextPnt,step] = lieb_opt_ls(TrialPnt,rho_ref,jp_ref,R,mass,Wee,mu_reg);  
            disp(sprintf('Iteration %d: G = %f [-> Gnew = %f, DeltaG = %g], step = %f',it,TrialPnt.lieb_fval,NextPnt.lieb_fval,NextPnt.lieb_fval-TrialPnt.lieb_fval,step))
            
            if NextPnt.lieb_fval < TrialPnt.lieb_fval
                num_decreases = num_decreases + 1;
                if num_decreases > 5
                    disp('Only allowing gradient steps from now on.')
                    only_grad_steps = true;
                end                
            end
        end
        
        disp(sprintf('  ||U|| = %f, ||A|| = %f, ||grad_U|| = %f, ||grad_A|| = %f',norm(TrialPnt.U),norm(TrialPnt.A),norm(TrialPnt.grad_U),norm(TrialPnt.grad_A)));
        disp(sprintf('  Egap = %f, m = %f, m_ref = %f', TrialPnt.Egap, m_trial, m_ref));

        % update trial potentials and densities
        TrialPnt = NextPnt;

        % concavity check
        lieb_concavity_check(OptHist,rho_ref,jp_ref,R,mass,Wee,mu_reg)
        
        % test convergence (zero gradient)
        grad_UA = [TrialPnt.grad_U; TrialPnt.grad_A];
        if norm(grad_UA) < conv_tol
            not_converged = false;
        end
        
        % alternative convergence check (zero gap, stagnated optimization)
        if (it > 50)
            EGAPS = [];
            GNRM = [];
            for k = 0:10
                gtmp = norm([OptHist(end-k).grad_U; OptHist(end-k).grad_A]);
                GNRM = [GNRM; gtmp];
                EGAPS = [EGAPS; OptHist(end-k).Egap];
            end
            if (max(abs(EGAPS)) < 1.0e-5) & (std(log(GNRM)) < 1.0e-4)
                disp('Zero gap and stagnated optimization. The maximum appears to be a non-differentiable cusp.');
                not_converged = false;
                
                %SFAC = [-0.2,-0.1,-0.05,-0.01,0,0.01,0.05,0.1,0.2,0.5];
                %GSTAG = 0*SFAC;
                %for si = 1:length(SFAC)
                %    Ustag = OptHist(end).U + SFAC(si) * OptHist(end).grad_U;
                %    Astag = OptHist(end).A + SFAC(si) * OptHist(end).grad_A;
                %    StagPnt = lieb_fun_eval(Ustag, Astag, rho_ref, jp_ref, R, mass, Wee, mu_reg);
                %    GSTAG(si) = StagPnt.lieb_fval;
                %end
                %
                %figure(10); clf; hold on;
                %plot(SFAC,GSTAG,'b.-');
                %xlabel('s');
                %ylabel('G(a + s g)');
                %title('Stagnation???');
                %error('Lieb optimization stagnated.')
            end
        end
    end

    disp('Converged!!!')
    Uopt = TrialPnt.U;
    Aopt = TrialPnt.A;
    
    if show_plots
        figure(24); clf; hold on;
        subplot(3,1,1); hold on;
        plot(Theta, TrialPnt.grad_U, 'k-');
        plot(Theta, TrialPnt.grad_A, 'b-');
        %plot(Theta, drho_ref, 'k--');
        %plot(Theta, Utrial-Atrial.^2/(2*mass), 'b--');
        axis tight;
        legend('grad U','grad A');
        xlabel('\theta');
        ylabel('Lieb gradients');
        title('Final output from Lieb opt.');
        subplot(3,1,2); hold on;
        rho_trial = (TrialPnt.grad_U/hR + mu_reg*TrialPnt.U + rho_ref);
        jp_trial = (TrialPnt.grad_A/hR + mu_reg*TrialPnt.A + jp_ref);
        plot(Theta, rho_trial, 'r-');
        plot(Theta, rho_ref, 'k--');
        plot(Theta, rho_trial - mu_reg*TrialPnt.U, 'mo');
        axis tight;
        %legend('jp (trial)','jp (ref)','A (trial)');
        xlabel('\theta');
        ylabel('density');
        subplot(3,1,3); hold on;
        plot(Theta, jp_trial, 'r-');
        plot(Theta, jp_ref, 'k--');
        axis tight;
        %legend('jp/rho (trial)','jp/rho (ref)');
        xlabel('\theta');
        ylabel('current density');
    
        figure(25); clf; hold on;
        G = [];
        for j = 1:length(OptHist)
            G = [G; OptHist(j).lieb_fval];
        end
        plot(G, 'k.-');
        xlabel('Iteration');
        ylabel('Lieb obj. fun. G');
        %figure(26); clf; hold on;
        %plot(log10(norm(OptHist(:).grad(1:Ngrid))), 'b*-');
        %plot(log10(norm(OptHist(:).grad((Ngrid+1):end))), 'r.-');
        %legend('density diff','curr. diff');
        %xlabel('Iteration');
        %ylabel('log_{10}( gradient norm )');
        %title('Lieb opt. gradient norms');
        %figure(27); clf; hold on;
        %plot(OptHist(:).m_value, 'k.-');
        %xlabel('Iteration');
        %ylabel('Average periodicity, m');
    end
end

function [Psi0,rho0,jp0,E0,Egap] = gs_1el(V,A,R,mass)
    Ngrid = length(V); 
    hstep = 2*pi/Ngrid;
    hR = hstep * R;
    
    % momentum operators (forward and backward)
    Pforw = -1i*(diag(ones(Ngrid-1,1), 1) - diag(ones(Ngrid,1))) / hR;
    Pforw(Ngrid,1) = -1i / hR;
    Psym = 0.5 * (Pforw + Pforw');
    Pback = Pforw';
    
    %PIforw = Pforw + diag(A);
    %PIback = Pback + diag(A);
    %Ham = 1/(2*mass) * PIback * PIforw + diag(V);
    Ham = (Pback*Pforw + diag(A)*Psym + Psym*diag(A) + diag(A.^2)) / (2*mass) + diag(V);
    Ham = 0.5 * (Ham + Ham');
    
    % brute force diagonalization
    [U,E] = eig(Ham);
    E = diag(E);
    
    [SortedE, IX] = sort(E);
    E = E(IX);
    U = U(:,IX);

    occfac = 2;  % factor for double occupation of orbital
    E0 = occfac * E(1);
    Egap = occfac*(E(2) - E(1));
    Psi0 = U(:,1) / sqrt(sum(abs(U(:,1)).^2) * hR);

    rho0 = occfac * abs(Psi0).^2;
    jp0 = occfac * 0.5*real(conj(Psi0).*(Pforw+Pback)*Psi0) / mass;
end

function [Psi,rho,jp,E,Egap] = gs_2el(V,A,W,R,mass)
    Ngrid = length(V);
    hstep = 2*pi/Ngrid;
    hR = hstep * R;
    Nroots = 3;
    
    OPTS.isreal = false;
    %OPTS.tol = 1e-17;
    [PsiVec, EVec] = eigs(@(x)apply_Ham_2el(x,V,A,W,R,mass), Ngrid^2, Nroots, 'sr', OPTS);

    % choose lowest symmetric state
    perm_sym = -1;
    ksel = 0;
    while (perm_sym < 0) & (ksel <= Nroots)
        % update counter
        ksel = ksel + 1;

        % extract data
        E = real(EVec(ksel));
        Psi = PsiVec(:,ksel);
        Psi = reshape(Psi, Ngrid, Ngrid);
        Psi = Psi / sqrt(sum(sum(abs(Psi).^2)) * hR^2);
    
        Egap = -666;
        if ksel < Nroots
            Egap = EVec(ksel+1) - EVec(ksel);
        end
    
        perm_sym = hR^2 * sum(sum(conj(Psi.') .* Psi));
    end
    
    if perm_sym < 0
        disp(sprintf('gs_2el(): Want lowest symmetric state, found only anti-symmetric states (%f). Cannot recover.',perm_sym))
        error('Antisymmetry!')
    elseif ksel > 1
        disp(sprintf('Warning: lowest state is anti-symmetric. Choosing state %d.',ksel))
    end
    
    GradPsi = (circshift(Psi,[-1,0]) - circshift(Psi,[1,0])) / (2*hR);    
    rho = 2 * sum(abs(Psi).^2,2) * hR;
    jp = 2 * real(sum(-1i*conj(Psi).*GradPsi,2) * hR);
end

function [Psi,rho,jp,E,Egap] = gs_2el_iter(V,A,W,R,mass)
    Ngrid = length(V);
    hstep = 2*pi/Ngrid;
    hR = hstep * R;
        
    % initial trial vector space
    [Orb_nonint,dum1,dum2,dum3,dum4] = gs_1el(V,A,R,mass);
    Psi_nonint = reshape(Orb_nonint * Orb_nonint.', Ngrid^2, 1);
    Trial = [Psi_nonint];
    %Trial = [];
    Theta = linspace(0,2*pi*(1-1/Ngrid),Ngrid);
    max_mtot = 5;
    for m = -max_mtot:max_mtot
        Orb = exp(m*1i*Theta)';
        Psi_m = reshape(Orb * Orb.', Ngrid^2, 1);
        Trial = [Trial, Psi_m];
    end
    
    ESEQ = [];
    ResSeq = [];
    gs_res_norm = +666.0;

    it = 0;
    while (it < 900) & (gs_res_norm > 1e-8)
        % update iteration counter
        it = it + 1;
        
        % add trial vectors
        if it > 2
            ResIt = [];
            for s = 1:2
                Xi1 = apply_Ham_2el(Trial(:,s),V,A,W,R,mass);
                w1 = Trial(:,s)' * Xi1 / (Trial(:,s)'*Trial(:,s));
                Res1 = Xi1 - real(w1)*Trial(:,s);
                rlen = norm(Res1);
                if rlen > 1e-8
                    %Res1 = (rand(Ngrid^2,1)-0.5) + 1i*(rand(Ngrid^2,1)-0.5);
                    %rlen = norm(Res1);
                    Trial = [Trial, Res1/rlen];
                end
                ResIt = [ResIt, rlen];
            end
            Eta = (rand(Ngrid^2,1)-0.5) + 1i*(rand(Ngrid^2,1)-0.5);
            Eta = Eta / sqrt(sum(abs(Eta).^2));
            Trial = [Trial, Eta];

            ResSeq = [ResSeq; ResIt];
            gs_res_norm = ResIt(1);
        else
            Xi1 = apply_Ham_2el(Trial(:,1),V,A,W,R,mass);
            Xi2 = apply_Ham_2el(Trial(:,2),V,A,W,R,mass);
            Eta = (rand(Ngrid^2,1)-0.5) + 1i*(rand(Ngrid^2,1)-0.5);
            Eta = Eta / sqrt(sum(abs(Eta).^2));
        
            Trial = [Trial, Xi1, Xi2, Eta];
        end
        
        % orthogonalize
        SubMetric = Trial' * Trial;
        [Uortho, Sigma] = eig(SubMetric);
        Sigma = diag(Sigma);
        INZ = find(Sigma > 1e-7);
        Uhalf = Uortho(:,INZ) * diag(1./sqrt(Sigma(INZ)));        
        Trial = Trial * Uhalf;
        
        % force permutation symmetric wave functions
        for s = 1:size(Trial,2)
            Xi = reshape(Trial(:,s), Ngrid, Ngrid);
            Xi = 0.5 * (Xi + Xi.');
            Trial(:,s) = reshape(Xi, Ngrid^2, 1);
        end
        
        % subspace Hamiltonian
        Ntrial = size(Trial,2);
        %ortho_error = norm(Trial'*Trial - eye(Ntrial))
        SubHam = zeros(Ntrial, Ntrial);
        for c = 1:Ntrial
            Hvc = apply_Ham_2el(Trial(:,c),V,A,W,R,mass);
            for r = 1:c
                SubHam(r,c) = Trial(:,r)' * Hvc;
                if r < c
                    SubHam(c,r) = SubHam(r,c)';
                end
            end
        end
        
        % rotate trial vector space
        [SubU, SubE] = eig(SubHam);
        [SortedE,Isub] = sort(real(diag(SubE)));
        SortedU = SubU(:,Isub);
        Trial = Trial * SortedU;
        
        %ortho_error2 = norm(Trial'*Trial - eye(Ntrial))
        ESEQ = [ESEQ, SortedE(1:2)];
        
        % truncate trial vector space
        if Ntrial > 20
            %disp(sprintf('Iteration %d: %d-dimensional trial vector space truncated.',it,Ntrial));
            Trial = Trial(:,1:3);
        end
    end
    
    Psi = reshape(Trial(:,1), Ngrid, Ngrid);
    Psi = Psi / sqrt(sum(sum(abs(Psi).^2)) * hR^2);
    GradPsi = (circshift(Psi,[-1,0]) - circshift(Psi,[1,0])) / (2*hR);

    rho = 2 * sum(abs(Psi).^2,2) * hR;
    jp = 2 * real(sum(-1i*conj(Psi).*GradPsi,2) * hR);
    E = SortedE(1);
    Egap = SortedE(2) - SortedE(1);    
    
    %%%
    if 0
        % display convergence info
        figure(21); clf; hold on;
        plot(ESEQ(1,:),'b.-');
        plot(ESEQ(2,:),'r-');
        axis tight;
        grid on;
        xlabel('Iteration');
        ylabel('Energy (int. sys.)');

        figure(22); clf; hold on;
        plot(log10(ResSeq(:,1)),'b.-');
        plot(log10(ResSeq(:,2)),'r-');
        axis tight;
        grid on;
        xlabel('Iteration');
        ylabel('log_{10} of residual norm');
    
        %Psi_sym = [norm(Psi - Psi.'), norm(Psi + Psi.')]
        %Psi_exc1_sym = [norm(Psi_exc1 - Psi_exc1.'), norm(Psi_exc1 + Psi_exc1.')]
    end
end

function pi_Psi = apply_pi(Psi,A,idim,hconj)
    if idim == 1
        Istep = [-1,0];
        Aop = repmat(A,1,size(Psi,2));
    else
        Istep = [0,-1];
        Aop = repmat(A',size(Psi,1),1);
    end
    Aop = Aop';  %% fulhack!
    fac = +1;
    if hconj
        Istep = -Istep;
        %fac = -1;
    end
    
    pi_Psi = -fac*1i*(circshift(Psi,Istep) - Psi) + Aop.*Psi;
end

function Hpsi = apply_Ham_2el(Psi,V,A,W,R,mass)
    Ngrid = length(V);
    hstep = 2*pi/Ngrid;
    hR = hstep * R;
    
    reshape_flag = 0;
    if size(Psi,1) ~= Ngrid
        reshape_flag = 1;
        Psi = reshape(Psi, Ngrid, Ngrid);
    end
    
    U = V + A.^2 / (2*mass);
    
    if 1
        Lap1 = (circshift(Psi,[1,0]) - 2*Psi + circshift(Psi,[-1,0])) / hR^2;
        Lap2 = (circshift(Psi,[0,1]) - 2*Psi + circshift(Psi,[0,-1])) / hR^2;
        Lap = Lap1 + Lap2;
    
        pA1_acom = 0*Psi;
        pA2_acom = 0*Psi;
        U_Psi = 0*Psi;
        for k1 = 1:Ngrid
            % scalar potential and diamagnetic term
            U_Psi(k1,:) = U_Psi(k1,:) + U(k1) * Psi(k1,:);
            U_Psi(:,k1) = U_Psi(:,k1) + U(k1) * Psi(:,k1);
        
            % paramagnetic terms
            k0 = (k1-1)*(k1 > 1) + Ngrid*(k1 == 1);
            k2 = (k1+1)*(k1 < Ngrid) + 1*(k1 == Ngrid);
        
            Ap1_Psi = -1i*A(k1)*(Psi(k2,:) - Psi(k0,:)) / (2*hR);        
            pA1_Psi = -1i*(A(k2)*Psi(k2,:) - A(k0)*Psi(k0,:)) / (2*hR);       
            pA1_acom(k1,:) = pA1_acom(k1,:) + Ap1_Psi + pA1_Psi;

            Ap2_Psi = -1i*A(k1)*(Psi(:,k2) - Psi(:,k0)) / (2*hR);        
            pA2_Psi = -1i*(A(k2)*Psi(:,k2) - A(k0)*Psi(:,k0)) / (2*hR);       
            pA2_acom(:,k1) = pA2_acom(:,k1) + Ap2_Psi + pA2_Psi;        
        end
        pA_acom = pA1_acom + pA2_acom;

        % add up everything
        Hpsi = (-Lap + pA_acom) / (2*mass) + U_Psi + W.*Psi;
    else
        % potential terms
        Uop = repmat(U,1,Ngrid);
        Uop = Uop + Uop.';
        Hpsi = Uop.*Psi + W.*Psi;

        % laplacian and gradient terms
        A1op = repmat(A,1,Ngrid);
        for particle_idx = 1:2
            if particle_idx == 1
                Phi = Psi;
            else
                % swap particle 1 and particle 2
                Phi = Psi.';
            end
            Lap1 = (circshift(Phi,[1,0]) - 2*Phi + circshift(Phi,[-1,0])) / hR^2;
            
            AP1phi = -1i*A1op.*(circshift(Phi,[-1,0]) - circshift(Phi,[1,0])) / (2*hR);
            A1phi = A1op .* Phi;
            PA1phi = -1i*(circshift(A1phi,[-1,0]) - circshift(A1phi,[1,0])) / (2*hR);
            
            if particle_idx == 1
                Hpsi = Hpsi + (-Lap1 + AP1phi + PA1phi) / (2*mass);
            else
                % remember to swap back 1 and 2 before adding to Hpsi!
                Hpsi = Hpsi + (-Lap1 + AP1phi + PA1phi).' / (2*mass);
            end
        end
    end
    
    % penalize non-symmetric wave functions
    %Hpsi = 10*(Psi - Psi.');
    %Hpsi = 0.5 * (Hpsi + Hpsi.');
    
    if reshape_flag
        Hpsi = reshape(Hpsi, Ngrid*Ngrid, 1);
    end
end
